<?php
namespace App\Helpers;

use App\models\Rute;
use App\models\Transit;

class TixtrainHelper {
    public static function getRute() {
        $data = Rute::all();

        return $data;
    }

    public static function getTransit() {
        $data = Transit::all();

        return $data;
    }
}
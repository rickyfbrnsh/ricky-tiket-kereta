<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Transit extends Model
{
    protected $table = 'ricky_tbl_transit';

    protected $primaryKey = 'ricky_id_transit';
}

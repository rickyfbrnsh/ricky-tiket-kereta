-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2020 at 01:43 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ricky_tiket_kereta`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ricky_tbl_detail_kereta`
--

CREATE TABLE `ricky_tbl_detail_kereta` (
  `ricky_id_detkereta` bigint(20) NOT NULL,
  `ricky_jml_gerbong` int(11) NOT NULL,
  `ricky_jml_kursi` int(11) NOT NULL,
  `ricky_id_kereta` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ricky_tbl_detail_transaksi`
--

CREATE TABLE `ricky_tbl_detail_transaksi` (
  `ricky_id_detail` bigint(20) NOT NULL,
  `ricky_nama_penumpang` varchar(30) NOT NULL,
  `ricky_no_kursi` varchar(3) NOT NULL,
  `ricky_id_trans` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ricky_tbl_kelas`
--

CREATE TABLE `ricky_tbl_kelas` (
  `ricky_id_kelas` int(11) NOT NULL,
  `ricky_nama_kelas` varchar(10) NOT NULL,
  `ricky_harga` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ricky_tbl_kereta`
--

CREATE TABLE `ricky_tbl_kereta` (
  `ricky_id_kereta` bigint(20) NOT NULL,
  `ricky_nama_kereta` varchar(50) NOT NULL,
  `ricky_id_kelas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ricky_tbl_rute`
--

CREATE TABLE `ricky_tbl_rute` (
  `ricky_id_rute` bigint(20) NOT NULL,
  `ricky_rute_awal` bigint(20) NOT NULL,
  `ricky_rute_akhir` bigint(20) NOT NULL,
  `ricky_transit` varchar(50) NOT NULL,
  `ricky_id_kereta` bigint(20) NOT NULL,
  `ricky_tgl` date NOT NULL,
  `ricky_harga` float NOT NULL,
  `ricky_jam_berangkat` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ricky_tbl_transaksi`
--

CREATE TABLE `ricky_tbl_transaksi` (
  `ricky_id_trans` bigint(20) NOT NULL,
  `ricky_kode` varchar(8) NOT NULL,
  `ricky_tgl` date NOT NULL,
  `ricky_id_kereta` bigint(20) NOT NULL,
  `ricky_jml_penumpang` int(11) NOT NULL,
  `ricky_rute_awal` bigint(20) NOT NULL,
  `ricky_rute_akhir` bigint(20) NOT NULL,
  `ricky_total` float NOT NULL,
  `ricky_user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ricky_tbl_transit`
--

CREATE TABLE `ricky_tbl_transit` (
  `ricky_id_transit` bigint(20) NOT NULL,
  `ricky_stasion` varchar(30) NOT NULL,
  `ricky_lokasi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ricky_tbl_users`
--

CREATE TABLE `ricky_tbl_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ricky_tbl_detail_kereta`
--
ALTER TABLE `ricky_tbl_detail_kereta`
  ADD PRIMARY KEY (`ricky_id_detkereta`),
  ADD KEY `ricky_id_detkereta` (`ricky_id_detkereta`),
  ADD KEY `ricky_id_kereta` (`ricky_id_kereta`);

--
-- Indexes for table `ricky_tbl_detail_transaksi`
--
ALTER TABLE `ricky_tbl_detail_transaksi`
  ADD PRIMARY KEY (`ricky_id_detail`),
  ADD KEY `ricky_id_trans` (`ricky_id_trans`);

--
-- Indexes for table `ricky_tbl_kelas`
--
ALTER TABLE `ricky_tbl_kelas`
  ADD PRIMARY KEY (`ricky_id_kelas`),
  ADD KEY `ricky_id_kelas` (`ricky_id_kelas`),
  ADD KEY `ricky_id_kelas_2` (`ricky_id_kelas`);

--
-- Indexes for table `ricky_tbl_kereta`
--
ALTER TABLE `ricky_tbl_kereta`
  ADD PRIMARY KEY (`ricky_id_kereta`),
  ADD KEY `ricky_id_kereta` (`ricky_id_kereta`),
  ADD KEY `ricky_id_kelas` (`ricky_id_kelas`);

--
-- Indexes for table `ricky_tbl_rute`
--
ALTER TABLE `ricky_tbl_rute`
  ADD PRIMARY KEY (`ricky_id_rute`),
  ADD KEY `ricky_rute_awal` (`ricky_rute_awal`),
  ADD KEY `ricky_rute_akhir` (`ricky_rute_akhir`),
  ADD KEY `ricky_id_transit` (`ricky_transit`),
  ADD KEY `ricky_id_kereta` (`ricky_id_kereta`);

--
-- Indexes for table `ricky_tbl_transaksi`
--
ALTER TABLE `ricky_tbl_transaksi`
  ADD PRIMARY KEY (`ricky_id_trans`),
  ADD KEY `ricky_rute_awal` (`ricky_rute_awal`),
  ADD KEY `ricky_rute_akhir` (`ricky_rute_akhir`),
  ADD KEY `ricky_id_pengguna` (`ricky_user_id`),
  ADD KEY `ricky_id_kereta` (`ricky_id_kereta`);

--
-- Indexes for table `ricky_tbl_transit`
--
ALTER TABLE `ricky_tbl_transit`
  ADD PRIMARY KEY (`ricky_id_transit`),
  ADD KEY `ricky_id_transit` (`ricky_id_transit`);

--
-- Indexes for table `ricky_tbl_users`
--
ALTER TABLE `ricky_tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ricky_tbl_detail_kereta`
--
ALTER TABLE `ricky_tbl_detail_kereta`
  MODIFY `ricky_id_detkereta` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ricky_tbl_detail_transaksi`
--
ALTER TABLE `ricky_tbl_detail_transaksi`
  MODIFY `ricky_id_detail` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ricky_tbl_kelas`
--
ALTER TABLE `ricky_tbl_kelas`
  MODIFY `ricky_id_kelas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ricky_tbl_kereta`
--
ALTER TABLE `ricky_tbl_kereta`
  MODIFY `ricky_id_kereta` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ricky_tbl_rute`
--
ALTER TABLE `ricky_tbl_rute`
  MODIFY `ricky_id_rute` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ricky_tbl_transaksi`
--
ALTER TABLE `ricky_tbl_transaksi`
  MODIFY `ricky_id_trans` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ricky_tbl_transit`
--
ALTER TABLE `ricky_tbl_transit`
  MODIFY `ricky_id_transit` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ricky_tbl_users`
--
ALTER TABLE `ricky_tbl_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ricky_tbl_detail_kereta`
--
ALTER TABLE `ricky_tbl_detail_kereta`
  ADD CONSTRAINT `ricky_tbl_detail_kereta_ibfk_1` FOREIGN KEY (`ricky_id_kereta`) REFERENCES `ricky_tbl_kereta` (`ricky_id_kereta`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ricky_tbl_detail_transaksi`
--
ALTER TABLE `ricky_tbl_detail_transaksi`
  ADD CONSTRAINT `ricky_tbl_detail_transaksi_ibfk_1` FOREIGN KEY (`ricky_id_trans`) REFERENCES `ricky_tbl_transaksi` (`ricky_id_trans`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ricky_tbl_kereta`
--
ALTER TABLE `ricky_tbl_kereta`
  ADD CONSTRAINT `ricky_tbl_kereta_ibfk_1` FOREIGN KEY (`ricky_id_kelas`) REFERENCES `ricky_tbl_kelas` (`ricky_id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ricky_tbl_rute`
--
ALTER TABLE `ricky_tbl_rute`
  ADD CONSTRAINT `ricky_tbl_rute_ibfk_1` FOREIGN KEY (`ricky_rute_awal`) REFERENCES `ricky_tbl_transit` (`ricky_id_transit`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ricky_tbl_rute_ibfk_2` FOREIGN KEY (`ricky_rute_akhir`) REFERENCES `ricky_tbl_transit` (`ricky_id_transit`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ricky_tbl_rute_ibfk_3` FOREIGN KEY (`ricky_id_kereta`) REFERENCES `ricky_tbl_kereta` (`ricky_id_kereta`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ricky_tbl_transaksi`
--
ALTER TABLE `ricky_tbl_transaksi`
  ADD CONSTRAINT `ricky_tbl_transaksi_ibfk_1` FOREIGN KEY (`ricky_id_kereta`) REFERENCES `ricky_tbl_kereta` (`ricky_id_kereta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ricky_tbl_transaksi_ibfk_2` FOREIGN KEY (`ricky_user_id`) REFERENCES `ricky_tbl_users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

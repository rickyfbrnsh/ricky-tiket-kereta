<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\models\Transit;

class Rute extends Model
{
    protected $table = 'ricky_tbl_rute';

    protected $primaryKey = 'ricky_id_rute';

    public function getTransitFirst()
    {
        return $this->hasOne(Transit::class, 'ricky_id_transit', 'ricky_rute_awal');
    }

    public function getTransitLast()
    {
        return $this->hasOne(Transit::class, 'ricky_id_transit', 'ricky_rute_akhir');
    }
}

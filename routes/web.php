<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('fe-landing-index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// routing group
Route::group(['prefix' => '/fe'], function() {
    Route::get('/', 'frontend\LandingController@index')->name('fe-landing-index');
    Route::get('/route', 'frontend\RouteController@index')->name('fe-route-index');
});
